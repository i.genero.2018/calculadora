# Programa que hace funciones de calculadora simple.
# Dos funciones: sumar y restar.
# Cada función acepta dos parámetros y devuelve su suma o su diferencia.
# El programa principal llama a esas funciones para sumar
# primero 1 y 2, y luego 3 y 4, mostrando el resultado en pantalla.
# A continuación restará primero 5 de 6, y luego 7 de 8, mostrando también los resultados
def sumar(num1, num2):   # Función para sumar números e imprimir el resultado
    print('{a} + {b} = {suma}'.format(a=num1, b=num2, suma=num1+num2))
    return num1 + num2
def restar(num1, num2):   # Función para restar números e imprimir el resultado
    print('{a} - {b} = {resta}'.format(a=num1, b=num2, resta=num1 - num2))
    return num1 - num2

if __name__ == "__main__":   # Programa principal
    sumar(1,2)
    sumar(3, 4)
    restar(6, 5)
    restar(8, 7)
